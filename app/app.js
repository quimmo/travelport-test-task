const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const routes = require('./routes');
const PlainError = require('./utils/plainError');

const responses = require('./responses/responses');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('*', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  next();
});

routes(app);

app.use((req, res, next) => {
  next(new PlainError(responses.NOT_FOUND));
});

app.use((err, req, res, next) => { // eslint-disable-line
  const result = Object.assign({}, responses[err.status], err);
  const {
    status,
    message,
    httpStatus,
  } = result;

  res.status = httpStatus;

  res.json({
    status,
    message,
  });
});

module.exports = app;
