const TwitterCache = require('../dataAccess/cache/twitter');
const TwitterRemoteDataAccess = require('../dataAccess/remote/twitter');
const {
  TWEETS_GET_OK,
} = require('../responses/responses');

class TwitterController {
  constructor() {
    this.twitterCache = new TwitterCache();
    this.twiiterApi = new TwitterRemoteDataAccess();
  }
  async getByHashTag(hashTag) {
    let tweets = await this.twitterCache.getFromCache(hashTag);
    if (tweets === null) {
      tweets = await this.twiiterApi.searchTweets(`#${hashTag}`);
      await this.twitterCache.cacheRequestResult(hashTag, tweets);
    }
    return Object.assign({}, TWEETS_GET_OK, { payload: JSON.parse(tweets) });
  }
}

module.exports = new TwitterController();
