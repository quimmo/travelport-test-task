const redisClient = require('../redis');

class TwitterCache {
  async cacheRequestResult(key, value) {
    return redisClient.setAsync(key, value);
  }

  async getFromCache(key) {
    return redisClient.getAsync(key);
  }
}

module.exports = TwitterCache;
