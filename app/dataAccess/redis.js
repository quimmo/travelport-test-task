const redis = require('redis');
const config = require('../utils/config');
const bluebird = require('bluebird');

const {
  host,
  port,
} = config.redis;

const client = redis.createClient({
  host,
  port,
});

client.on('error', (err) => {
  console.log(err);
});

module.exports = bluebird.promisifyAll(client);
