const TwitterApi = require('twitter-node-client').Twitter;
const config = require('../../utils/config');

class TwitterRemoteDataAccess {
  constructor() {
    this.twitterApi = new TwitterApi(config.twitter);
    this.searchCount = 25;
  }
  async searchTweets(query) {
    return new Promise((resolve, reject) => {
      this.twitterApi.getSearch(
        {
          q: query,
          count: this.searchCount,
        },
        error => reject(error),
        result => resolve(result),
      );
    });
  }
}


module.exports = TwitterRemoteDataAccess;
