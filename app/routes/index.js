const twitter = require('./twitter');

module.exports = (app) => {
  app.use('/twitter', twitter);
};

