const express = require('express');
const wrapper = require('../middlewares/routeWrapper');
const twitterController = require('../controllers/twitterController');

const route = express.Router();

route.get('/hashtag', wrapper(async req => twitterController.getByHashTag(req.query.tag)));

module.exports = route;
